import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import Navbar from './Components/layout/Navbar';
import Token from './routes/token'
import Asset from './routes/asset'
import { BrowserRouter, Route } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Navbar/>
          <Route path="/asset" component={Asset} />
          <Route path="/token" component={Token} />
          <ToastContainer />
        </div>

      </BrowserRouter>
    );
  }
}

export default App;
