import React, { Fragment } from "react";
import { Formik, Field } from "formik";
import { toast } from 'react-toastify';
import ApiService from "../../services/services";


const initialState = {
  name: "",
  symbol: "",
  supply: "",
  Private_key:""
};
function InnerForm(props) {
  return (
    <Fragment>
      <Formik
        initialValues={initialState}
        onSubmit={(values, { props, setSubmitting, setErrors }) => {
          ApiService.createToken(values).then(payload=>{
          setSubmitting(true)
          toast.success("Token Created successfully")
          console.log(payload)
          }).catch(err=>{
            setSubmitting(false)
            toast.error(err.data && err.data.msg ? err.data.msg : 'Error')
})
        }}
      >
        {props => (
          <form onSubmit={props.handleSubmit} >

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Token Name</label>
            <Field
              type="text"
              placeholder="Token"
              onChange={props.handleChange}
              name="name"
              value={props.values.name}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Token Symbol</label>
            <Field
              type="text"
              placeholder="symbol"
              onChange={props.handleChange}
              name="symbol"
              value={props.values.symbol}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Token Supply</label>
            <Field
              type="number"
              placeholder="Token Supply"
              onChange={props.handleChange}
              name="supply"
              value={props.values.supply}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Private key</label>
            <Field
              type="text"
              placeholder="Private key"
              onChange={props.handleChange}
              name="Private_key"
              value={props.values.Private_key}
              className="form-control"
            />
          </div>

            <button className="btn btn-primary col-md-12"
              type="submit"
              disabled={!props.dirty && !props.isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
    </Fragment>
  );
}

export default InnerForm;
