import React, { Fragment } from "react";
import { Formik, Field } from "formik";
import { toast } from 'react-toastify';
import ApiService from "../../services/services";
import bootstrap from 'bootstrap/dist/css/bootstrap.css'

const initialState = {
  name: "",
  symbol: "",
  supply: "",
  address:"",
  owner:"",
  owner_social_media:"",
  proof_of_title:"",
  notarization_certificate:"",
  property_description:"",
  Private_key:"",

};
function InnerForm(props) {
  return (
    <Fragment>
      <Formik
        initialValues={initialState}
        onSubmit={(values, { props, setSubmitting, setErrors }) => {
          ApiService.createAsset({address:values.address, owner: values.owner, owner_social_media: values.owner_social_media, proof_of_title:values.proof_of_title, Private_key: values.Private_key}).then(payload=>{
          setSubmitting(true)
          toast.success("Asset Created successfully")
          console.log(payload)
          }).catch(err=>{
            setSubmitting(false)
            toast.error(err.data && err.data.msg ? err.data.msg : 'Error')
})
        }}
      >
        {props => (
          <form onSubmit={props.handleSubmit} >

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Token Name</label>
            <Field
              type="text"
              placeholder="Token"
              onChange={props.handleChange}
              name="name"
              value={props.values.name}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Token Symbol</label>
            <Field
              type="text"
              placeholder="symbol"
              onChange={props.handleChange}
              name="symbol"
              value={props.values.symbol}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Token Supply</label>
            <Field
              type="number"
              placeholder="Token Supply"
              onChange={props.handleChange}
              name="supply"
              value={props.values.supply}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Address</label>
            <Field
              type="text"
              placeholder="Address"
              onChange={props.handleChange}
              name="address"
              value={props.values.address}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Owner</label>
            <Field
              type="text"
              placeholder="Owner"
              onChange={props.handleChange}
              name="owner"
              value={props.values.owner}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Owner Social Media</label>
            <Field
              type="text"
              placeholder="Owner Social Media"
              onChange={props.handleChange}
              name="owner_social_media"
              value={props.values.owner_social_media}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Proof Of Title</label>
            <Field
              type="text"
              placeholder="Proof Of Title"
              onChange={props.handleChange}
              name="proof_of_title"
              value={props.values.proof_of_title}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Notarization Certificate</label>
            <Field
              type="text"
              placeholder="Notarization Certificate"
              onChange={props.handleChange}
              name="notarization_certificate"
              value={props.values.notarization_certificate}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Property Description</label>
            <Field
              type="text"
              placeholder="Property Description"
              onChange={props.handleChange}
              name="property_description"
              value={props.values.property_description}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Private key</label>
            <Field
              type="text"
              placeholder="Private key"
              onChange={props.handleChange}
              name="Private_key"
              value={props.values.Private_key}
              className="form-control"
            />
          </div>

            <button className="btn btn-primary col-md-12"
              type="submit"
              disabled={!props.dirty && !props.isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
    </Fragment>
  );
}

export default InnerForm;
